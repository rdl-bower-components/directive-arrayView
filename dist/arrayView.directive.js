/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};

/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {

/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId])
/******/ 			return installedModules[moduleId].exports;

/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			exports: {},
/******/ 			id: moduleId,
/******/ 			loaded: false
/******/ 		};

/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);

/******/ 		// Flag the module as loaded
/******/ 		module.loaded = true;

/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}


/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;

/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;

/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";

/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ function(module, exports, __webpack_require__) {

	module.exports = __webpack_require__(1);


/***/ },
/* 1 */
/***/ function(module, exports) {

	'use strict';

	Object.defineProperty(exports, "__esModule", {
	  value: true
	});

	var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

	exports.ArrayViewDirective = ArrayViewDirective;

	function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

	angular.module('rdl.arrayView', ['angularMoment']).directive('rdlArrayView', ArrayViewDirective);

	function ArrayViewDirective() {
	  'ngInject';

	  var directive = {
	    restrict: 'E',
	    template: '<div class="table-responsive"><table class="array-view table table-striped"><thead><tr><th ng-repeat="col in vm.config" class="{{col.columnClass}}" ng-class="{\'text-center\' : col.textCenter !== undefined}" ng-click="vm.updateSortOrder(col.sortName)"><span translate>{{col.header}}</span> <span ng-if="col.sortName !== undefined" ng-show="vm.sortType === col.sortName" class="fa" ng-class="(vm.sortAsc) ? \'fa-caret-up\': \'fa-caret-down\'"></span></th><th ng-if="vm.openedCallback !== undefined" class="open-column"></th><th ng-if="vm.editedCallback !== undefined" class="edit-column"></th><th ng-if="vm.deletedCallback !== undefined" class="delete-column"></th></tr></thead><tbody><tr ng-repeat="(rowIndex, row) in vm.rows"><td ng-repeat="col in vm.config" class="vert-align" ng-class="{\'text-center\' : col.textCenter !== undefined, \'text-capitalize\': col.textCapitalize !== undefined}"><a ng-if="col.type === \'link\'" title="{{vm.getValue(row, col.name)}}" href="{{vm.getValue(row, col.url)}}"><span ng-bind-html="row[col.name]"></span></a><span ng-if="col.type === \'text\'">{{vm.limitTo(vm.translate(col.translationPrefix, vm.getValue(row, col.name), col.limitTo))}}</span><a ng-if="col.type === \'image\'" ng-class="col.class" uib-popover-html="vm.imagePopup(vm.getValue(row, col.name))" popover-trigger="mouseenter" type="button"><img ng-src="{{vm.getValue(row, col.name)}}"></a><span ng-if="col.type === \'array\'">{{vm.getValue(row, col.name).join(\', \')}}</span><span ng-if="col.type === \'translatedArray\'">{{vm.translateArray(row, col.name, col.translationPrefix)}}</span><span ng-if="col.type === \'arrayOfObject\'">{{vm.getValueOfObject(row, col.name, col.field).join(\', \')}}</span><i ng-if="col.type === \'fa\'" class="fa" aria-hidden="true" ng-class="vm.getValue(row, col.name)"></i><i ng-if="col.type === \'boolean\' && vm.getValue(row, col.name) === true" class="fa fa-check" aria-hidden="true"></i> <i ng-if="col.type === \'boolean\' && vm.getValue(row, col.name) !== true" class="fa fa-times" aria-hidden="true"></i><span ng-if="col.type === \'date\'" am-time-ago="vm.getValue(row, col.name)" ng-attr-title="{{(vm.getValue(row, col.name)) | amDateFormat:\'LLLL\'}}"></span><small ng-if="col.type === \'score\'" class="text-muted">{{vm.getValue(row, col.name) | number : 2}}</small><a ng-if="col.type === \'button\' && vm.getValue(row, col.url) != \'\'" class="btn" href="{{vm.getValue(row, col.url)}}" ng-class="vm.buttonClass(col, row)"><span translate>{{col.name}}</span></a><a ng-if="col.type === \'buttonWithCallback\'" class="btn" ng-click="vm.callCallback(col, row, rowIndex)" ng-class="vm.buttonClass(col, row)"><span translate>{{col.name}}</span></a></td><td ng-if="vm.openedCallback !== undefined" class="vert-align"><a class="btn btn-eye" ng-click="vm.openRow(row, $index)"><i class="fa fa-trash" aria-hidden="true"></i></a></td><td ng-if="vm.editedCallback !== undefined" class="vert-align"><a class="btn btn-primary" ng-click="vm.editRow(row, $index)"><i class="fa fa-pencil" aria-hidden="true"></i></a></td><td ng-if="vm.deletedCallback !== undefined" class="vert-align"><a class="btn btn-danger" ng-click="vm.deleteRow($index)"><i class="fa fa-trash" aria-hidden="true"></i></a></td></tr><tr ng-if="vm.addedCallback !== undefined"><td ng-repeat="col in vm.config" class="vert-align" ng-class="{\'text-center\' : col.textCenter !== undefined, \'text-capitalize\': col.textCapitalize !== undefined}"><input ng-if="col.type !== \'button\'" class="form-control" ng-model="vm.editableModel[$index]"></td><td class="vert-align"><a class="btn btn-primary" ng-click="vm.addRow()"><span translate>{{vm.addText}}</span></a></td></tr></tbody></table></div>',
	    scope: {
	      rows: "=",
	      config: "=",
	      addedCallback: "&?",
	      deletedCallback: "&?",
	      editedCallback: "&?",
	      openedCallback: "&?",
	      sortedCallback: "&?",
	      addText: "@?",
	      defaultSortType: "@?",
	      defaultSortAsc: "@?"

	    },
	    controller: ArrayViewController,
	    controllerAs: 'vm',
	    bindToController: true
	  };

	  return directive;
	}

	var ArrayViewController = function () {
	  ArrayViewController.$inject = ["$filter", "$sce"];
	  function ArrayViewController($filter, $sce) {
	    'ngInject';

	    _classCallCheck(this, ArrayViewController);

	    this.$filter = $filter;
	    this.$sce = $sce;
	    this.sortType = this.defaultSortType !== undefined ? this.defaultSortType : null;
	    this.sortAsc = this.defaultSortAsc !== undefined ? String(this.defaultSortAsc) === "true" : true;
	    this.defaultSortType = this.sortType;
	    this.defaultSortAsc = this.sortAsc;
	    this.editableModel = new Array(this.config.length);
	    this.addText = this.addText !== undefined ? this.addText : 'Add';
	    this.trusted = [];
	  }

	  _createClass(ArrayViewController, [{
	    key: 'updateSortOrder',
	    value: function updateSortOrder(type) {
	      if (type === undefined || this.sortedCallback === undefined) {
	        return;
	      }
	      this.defaultSortAsc = String(this.defaultSortAsc) === "true";
	      if (this.sortType !== type) {
	        this.sortType = type;
	        this.sortAsc = this.defaultSortAsc;
	        this.sortedCallback({ sortType: this.sortType, sortAsc: this.sortAsc });
	      } else if (this.sortAsc === this.defaultSortAsc) {
	        this.sortAsc = !this.defaultSortAsc;
	        this.sortedCallback({ sortType: this.sortType, sortAsc: this.sortAsc });
	      } else {
	        this.sortType = this.defaultSortType;
	        this.sortAsc = this.defaultSortAsc;
	        this.sortedCallback({ sortType: this.sortType, sortAsc: this.sortAsc });
	      }
	    }
	  }, {
	    key: 'getValue',
	    value: function getValue(row, name) {
	      var items = name.split('.');
	      var value = row[items[0]];
	      for (var i = 1; i < items.length; ++i) {
	        if (!angular.isDefined(value) || value === null || angular.isUndefined(value[items[i]])) {
	          return '';
	        }
	        value = value[items[i]];
	      }
	      return value;
	    }
	  }, {
	    key: 'getValueOfObject',
	    value: function getValueOfObject(row, name, field) {
	      var fields = field.split('.');
	      return this.getValue(row, name).map(function (obj) {
	        var value = obj;
	        var _iteratorNormalCompletion = true;
	        var _didIteratorError = false;
	        var _iteratorError = undefined;

	        try {
	          for (var _iterator = fields[Symbol.iterator](), _step; !(_iteratorNormalCompletion = (_step = _iterator.next()).done); _iteratorNormalCompletion = true) {
	            var f = _step.value;

	            value = value[f];
	          }
	        } catch (err) {
	          _didIteratorError = true;
	          _iteratorError = err;
	        } finally {
	          try {
	            if (!_iteratorNormalCompletion && _iterator.return) {
	              _iterator.return();
	            }
	          } finally {
	            if (_didIteratorError) {
	              throw _iteratorError;
	            }
	          }
	        }

	        return value;
	      });
	    }
	  }, {
	    key: 'limitTo',
	    value: function limitTo(text, _limitTo) {
	      if (angular.isDefined(_limitTo) && text.length > _limitTo) {
	        return text.slice(0, _limitTo) + '...';
	      }
	      return text;
	    }
	  }, {
	    key: 'translate',
	    value: function translate(prefix, string) {
	      if (prefix === null || prefix === undefined) {
	        return string;
	      }
	      var res = this.$filter('translate')(prefix + string);
	      if (res.startsWith(prefix)) {
	        return string;
	      }
	      return res;
	    }
	  }, {
	    key: 'setValue',
	    value: function setValue(row, name, value) {
	      var items = name.split('.');
	      var item = row;
	      for (var i = 0; i < items.length - 1; ++i) {
	        if (item[items[i]] === undefined) {
	          item[items[i]] = {};
	        }
	        item = item[items[i]];
	      }
	      item[items[items.length - 1]] = value;
	    }
	  }, {
	    key: 'addRow',
	    value: function addRow() {
	      if (this.addedCallback !== undefined) {
	        this.addedCallback({ item: this.editableModel });
	      }
	    }
	  }, {
	    key: 'deleteRow',
	    value: function deleteRow(index) {
	      if (this.deletedCallback !== undefined) {
	        this.deletedCallback({ index: index });
	      }
	    }
	  }, {
	    key: 'editRow',
	    value: function editRow(item, index) {
	      if (this.editedCallback !== undefined) {
	        this.editedCallback({ item: item, index: index });
	      }
	    }
	  }, {
	    key: 'openRow',
	    value: function openRow(item, index) {
	      if (this.openedCallback !== undefined) {
	        this.openedCallback({ item: item, index: index });
	      }
	    }
	  }, {
	    key: 'arrayToString',
	    value: function arrayToString(array) {
	      var res = '';
	      array.forEach(function (element) {
	        res += element + ', ';
	      }, this);

	      return res.slice(0, -2);
	    }
	  }, {
	    key: 'buttonClass',
	    value: function buttonClass(col, row) {
	      var c = angular.isDefined(col.class) ? col.class : 'btn-primary';
	      if (this.isDisabled(col, row)) {
	        c += ' disabled';
	      }
	      return c;
	    }
	  }, {
	    key: 'imagePopup',
	    value: function imagePopup(url) {

	      var html = '<img src="' + url + '"/>';
	      this.trusted[html] || (this.trusted[html] = this.$sce.trustAsHtml(html));
	      return this.trusted[html];
	    }
	  }, {
	    key: 'callCallback',
	    value: function callCallback(col, item, index) {
	      if (col.callback !== undefined) {
	        col.callback(col.callbackScope, item, index);
	      }
	    }
	  }, {
	    key: 'isDisabled',
	    value: function isDisabled(col, item) {
	      if (col.isDisabled !== undefined) {
	        return col.isDisabled(col.callbackScope, item);
	      }
	      return false;
	    }
	  }, {
	    key: 'translateArray',
	    value: function translateArray(row, name, prefix) {
	      var _this = this;

	      var array = this.getValue(row, name);
	      if (!Array.isArray(array)) {
	        return "";
	      }
	      var result = ',';
	      array.forEach(function (element) {
	        result += ', ' + _this.$filter('translate')(prefix + element);
	      });
	      return result.substr(2);
	    }
	  }]);

	  return ArrayViewController;
	}();

/***/ }
/******/ ]);