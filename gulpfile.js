'use strict';

var path = require('path');
var gulp = require('gulp');
var flatten = require('gulp-flatten');
var webpack = require('webpack-stream');
var named = require('vinyl-named');
var embedTemplates = require('gulp-angular-embed-templates');
var gulpPlugins = require('gulp-load-plugins')();
var clean = require('gulp-clean');

const paths = {
  src: 'src',
  dest: 'dist',
  tmp: 'tmp'
};

function webpackWrapper() {
  var webpackOptions = {
    watch: false,
    module: {
      loaders: [{ test: /\.js$/, exclude: /node_modules/, loaders: ['ng-annotate', 'babel-loader?presets[]=es2015']}]
    }
  };

  var webpackChangeHandler = function(err, stats) {
    if(err) {
      conf.errorHandler('Webpack')(err);
    }
    gulpPlugins.util.log(stats.toString({
      colors: gulpPlugins.util.colors.supportsColor,
      chunks: false,
      hash: false,
      version: false
    }));
  };

  return gulp.src([ path.join(paths.tmp, '/**/*.js') ])
    .pipe(named())
    .pipe(webpack(webpackOptions, null, webpackChangeHandler))
    .pipe(gulp.dest(paths.dest));
}

gulp.task('styles', () => {
  return gulp.src([ path.join(paths.src, '/**/*.scss') ])
    .pipe(gulpPlugins.sass.sync().on('error', gulpPlugins.sass.logError))
    .pipe(flatten())
    .pipe(gulp.dest(paths.dest));
});

gulp.task('templates', () => {
  return gulp.src([ path.join(paths.src, '/**/*.js') ])
    .pipe(embedTemplates())
    .pipe(gulp.dest(paths.tmp));
});

gulp.task('scripts', ['templates'], () => {
  return webpackWrapper();
});

gulp.task('clean', function () {
  return gulp.src([path.join(paths.dest, '/'), path.join(paths.tmp, '/')])
    .pipe(clean());
});

gulp.task('build', ['scripts', 'styles']);

gulp.task('default', ['clean'], () => {
  gulp.start('build');
});
