Array view
==========

# Description
Array view that support different types for the columns like boolean, 
font awesome icon, array of string, etc. It allows to make quickly and easily 
a table.

# Install

    bower install git@gitlab.com:rdl-bower-components/directive-arrayView.git --save

Then, add `rdl.arrayView` in your index.module.js

# Exemple
```html
<rrddll-array-view  
    rows="ctrl.rows" 
    config="ctrl.config"
    added-callback="ctrl.addItem(item)"
    deleted-callback="ctrl.deleteItem(index)"
    edited-callback="ctrl.editItem(item, index)"
    opened-callback="ctrl.openItem(item, index)"
    sorted-callback="ctrl.onSorted(sortType, sortAsc)"
    default-sort-type="_score"
    default-sort-asc="false"
    add-text="action.add">
</bootstart-array-view>
```

# Directive attributes

## config (`required`)
    
The configuration of the different columns. The supported types are listed below :

### Array 
```javascript
{
  header: 'header.myArray',    //Header of the column, will be translated
  name: '_source.myValues',    //String array to display - Can be in a sub-object like myObject1.myObject2.myValues
  type: 'array',
  sortName: 'myValues',        //If not present, the columns cannot be sorted
  textCenter: true,            //If not present, the header and the value are aligned on the left
  textCapitalize: true,        //If present, the first letter of the value is capitalized
  column-class: 'my-column'    //If present, the class of the th will be the given value
}
```
    
### ArrayOfObject
```javascript
{
  header: 'header.myArray',    //Header of the column, will be translated
  name: '_source.myValues',    //Array to display - Can be in a sub-object like myObject1.myObject2.myValues
  field: 'item.field',         //Value inside each items to merge - Can be in a sub-object like myObject1.myObject2.myValue
  type: 'arrayOfObject',
  sortName: 'myValues',        //If not present, the columns cannot be sorted
  textCenter: true,            //If not present, the header and the value are aligned on the left
  textCapitalize: true,        //If present, the first letter of the value is capitalized
  column-class: 'my-column'    //If present, the class of the th will be the given value
}
```
    
### Button
```javascript
{
  header: 'header.myButton',   //Header of the column, will be translated
  name: '_source.name',        //Title of the button - Can be in a sub-object like myObject1.myObject2.value - will be translated
  url: '_source.url',          //Url of the button - Can be in a sub-object like myObject1.myObject2.value
  type: 'button',
  callbackScope: this,         //Scope to put in the first argument of the isDisabled function
  sortName: 'button',          //If not present, the columns cannot be sorted
  isDisabled: this.myFct,      //If present and if the given method return true, the item is disabled.
                               //The method has to be like "myFct(scope, item)".
  textCenter: true,            //If not present, the header and the value are aligned on the left
  textCapitalize: true,        //If present, the first letter of the value is capitalized
  class: 'btn-class',          //If present, the class of the button will be the given one, otherwise, it will be 'btn-primary' 
  column-class: 'my-column'    //If present, the class of the th will be the given value
}
```
    
### ButtonWithCallback
```javascript
{
  header: 'header.myButton',   //Header of the column, will be translated
  name: '_source.name',        //Title of the button - Can be in a sub-object like myObject1.myObject2.value - will be translated
  callback: this.onClick,      //Method to be called when the user released the button 
                               //The method has to be like "onClick(scope, item)".
  callbackScope: this,         //Scope to put in the first argument of the callback and the isDisabled function
  type: 'buttonWithCallback',
  sortName: 'button',          //If not present, the columns cannot be sorted
  isDisabled: this.myFct,      //If present and if the given method return true, the item is disabled.
                               //The method has to be like "myFct(scope, item)".
  textCenter: true,            //If not present, the header and the value are aligned on the left
  textCapitalize: true,        //If present, the first letter of the value is capitalized
  class: 'btn-class',          //If present, the class of the button will be the given one, otherwise, it will be 'btn-primary'
  column-class: 'my-column'    //If present, the class of the th will be the given value
}
```
    
### Boolean
```javascript
{
  header: 'header.myBoolean',  //Header of the column, will be translated
  name: '_source.myValue',     //Boolean of the link - Can be in a sub-object like myObject1.myObject2.myValue
  type: 'boolean',
  sortName: 'myValue'          //If not present, the columns cannot be sorted
  textCenter: true,            //If not present, the text in the column are aligned on the left
  column-class: 'my-column'    //If present, the class of the th will be the given value
}
```
    
### Date
```javascript
{
  header: 'header.myDate',  //Header of the column, will be translated
  name: '_source.date',     //Date to display with moment - will be translated
  type: 'date',
  sortName: 'date',         //If not present, the columns cannot be sorted
  textCenter: true,         //If not present, the header and the value are aligned on the left
  textCapitalize: true,     //If present, the first letter of the value is capitalized
  column-class: 'my-column' //If present, the class of the th will be the given value
}
```
    
### Font awesome
```javascript
{
  header: 'header.myDate', //Header of the column, will be translated
  name: '_source.icon',    //Font awesome class to use
  type: 'fa',
  sortName: 'icon',        //If not present, the columns cannot be sorted
  textCenter: true,        //If not present, the header and the value are aligned on the left
  column-class: 'my-column'//If present, the class of the th will be the given value
}
```
    
### Image 
```javascript
{
  header: 'header.myText', //Header of the column, will be translated
  name: '_source.name',    //Value to display as a text - Can be in a sub-object like myObject1.myObject2.value - will be translated
  type: 'image',
  textCenter: true,        //If not present, the header and the value are aligned on the left
  column-class: 'my-column'//If present, the class of the th will be the given value
  class: 'img-class',      //If present, the class of the image will be the given one, otherwise, it will be '' 
}
```

### Link
```javascript
{
  header: 'header.myLink', //Header of the column, will be translated
  name: '_source.name',    //Title of the link - Can be in a sub-object like myObject1.myObject2.value - will be translated
  url: '_source.url',      //Url of the link - Can be in a sub-object like myObject1.myObject2.value
  type: 'link',
  sortName: 'url',         //If not present, the columns cannot be sorted
  textCenter: true,        //If not present, the header and the value are aligned on the left
  textCapitalize: true,    //If present, the first letter of the value is capitalized
  column-class: 'my-column'//If present, the class of the th will be the given value
}
```
    
### Score
```javascript
{
  header: 'header.myScore', //Header of the column, will be translated
  name: '_score',           //Score to display - Can be in a sub-object like myObject1.myObject2.score
  type: 'score',
  sortName: '_score',       //If not present, the columns cannot be sorted
  textCenter: true,         //If not present, the header and the value are aligned on the left
  column-class: 'my-column' //If present, the class of the th will be the given value
}
```
    
### Text 
```javascript
{
  header: 'header.myText', //Header of the column, will be translated
  name: '_source.name',    //Value to display as a text - Can be in a sub-object like myObject1.myObject2.value - will be translated
  type: 'text',
  sortName: 'name',        //If not present, the columns cannot be sorted
  limitTo: 50,             //If not present, the columns is not trimmed
  textCenter: true,        //If not present, the header and the value are aligned on the left
  textCapitalize: true,    //If present, the first letter of the value is capitalized
  column-class: 'my-column'//If present, the class of the th will be the given value
  textCapitalize: true,    //If present, the first letter of the value is capitalized
  translationPrefix: 'app.'//If present, it will be add as a prefix for the traduction
}
```
    
### TranslatedArray 
```javascript
{
  header: 'header.myArray',    //Header of the column, will be translated
  name: '_source.myValues',    //String array to display - Can be in a sub-object like myObject1.myObject2.myValues
  type: 'translatedArray',
  sortName: 'myValues',        //If not present, the columns cannot be sorted
  textCenter: true,            //If not present, the header and the value are aligned on the left
  textCapitalize: true,        //If present, the first letter of the value is capitalized
  translationPrefix: 'app.'    //If present, it will be add as a prefix for the traduction
}
```

## rows (`required`)

The data displayed in the view. The structure has to be an array. Items of 
the array can be anything. The type and the location of the data in the items
are defined in the `config` object.

## added-callback
    
Callback called when the user click on the "add" button. If it is not
present, the add row and the add button are hidden.
```javascript
onClickOnDelete(item) {
  ...
}
```

## deleted-callback
    
Callback called the user click on the "delete" button. If it is not
present, the delete buttons are hidden.
```javascript
onClickOnDelete(index) {
  ...
}
```

## opened-callback
    
Callback called the user click on the "open" button. If it is not
present, the open buttons are hidden.
```javascript
onClickOnOpen(item, index) {
  ...
}
```

## edited-callback
    
Callback called the user click on the "edit" button. If it is not
present, the edit buttons are hidden.
```javascript
onClickOnEdit(item, index) {
  ...
}
```

## sorted-callback
    
Callback called when the user click on a sortable columns. The callback has to 
be like :
```javascript
onSorted(sortType, sortAsc) {
  ...
}
```

## default-sort-type
The default sort columns. By default, no columns are sorted.

## default-sort-asc
The default sort order. By default, the value is `true`.

## add-text

The text of the "add" button - will be translated.
By default the text is `Add`.