angular.module('rdl.arrayView', ['angularMoment']).directive('rdlArrayView', ArrayViewDirective);

export function ArrayViewDirective() {
  'ngInject';

  let directive = {
    restrict: 'E',
    templateUrl: 'arrayView.html',
    scope: {
      rows: "=",
      config: "=",
      addedCallback: "&?",
      deletedCallback: "&?",
      editedCallback: "&?",
      openedCallback: "&?",
      sortedCallback: "&?",
      addText: "@?",
      defaultSortType: "@?",
      defaultSortAsc: "@?"

    },
    controller: ArrayViewController,
    controllerAs: 'vm',
    bindToController: true
  };

  return directive;
}

class ArrayViewController {
  constructor($filter, $sce) {
    'ngInject';

    this.$filter = $filter;
    this.$sce = $sce;
    this.sortType = (this.defaultSortType !== undefined) ? this.defaultSortType : null;
    this.sortAsc = (this.defaultSortAsc !== undefined) ? String(this.defaultSortAsc) === "true" : true;
    this.defaultSortType = this.sortType;
    this.defaultSortAsc = this.sortAsc;
    this.editableModel = new Array(this.config.length);
    this.addText = (this.addText !== undefined) ? this.addText : 'Add';
    this.trusted = [];
  }

  updateSortOrder(type) {
    if (type === undefined || this.sortedCallback === undefined) {
      return;
    }
    this.defaultSortAsc = String(this.defaultSortAsc) === "true";
    if (this.sortType !== type) {
      this.sortType = type;
      this.sortAsc = this.defaultSortAsc;
      this.sortedCallback({sortType:this.sortType, sortAsc:this.sortAsc});
    }
    else if (this.sortAsc ===  this.defaultSortAsc) {
      this.sortAsc = !this.defaultSortAsc;
      this.sortedCallback({sortType:this.sortType, sortAsc:this.sortAsc});
    }
    else {
      this.sortType = this.defaultSortType;
      this.sortAsc = this.defaultSortAsc;
      this.sortedCallback({sortType:this.sortType, sortAsc:this.sortAsc});
    }
  }

  getValue(row, name) {
    var items = name.split('.');
    var value = row[items[0]];
    for (var i = 1 ; i < items.length ; ++i) {
      if (!angular.isDefined(value) || value === null || angular.isUndefined(value[items[i]])) {
        return '';
      }
      value = value[items[i]];
    }
    return value;
  }

  getValueOfObject(row, name, field) {
      let fields = field.split('.');
      return this.getValue(row, name).map(obj => {
          let value = obj;
          for(let f of fields) {
              value = value[f];
          }
          return value;
      });
  }
  
  limitTo(text, limitTo) {
      if (angular.isDefined(limitTo) && text.length > limitTo) {
          return text.slice(0, limitTo) + '...';
      }
      return text;
  }

  translate(prefix, string) {
    if (prefix === null || prefix === undefined) {
      return string;
    }
    let res = this.$filter('translate')(prefix + string);
    if (res.startsWith(prefix)) {
      return string;
    }
    return res;
  }

  setValue(row, name, value) {
    var items = name.split('.');
    var item = row;
    for (var i = 0 ; i < items.length - 1 ; ++i) {
      if (item[items[i]] === undefined) {
        item[items[i]] = {};
      }
      item = item[items[i]];
    }
    item[items[items.length - 1]] = value;
  }

  addRow() {
    if (this.addedCallback !== undefined) {
      this.addedCallback({item: this.editableModel});
    }
  }

  deleteRow(index) {
    if (this.deletedCallback !== undefined) {
      this.deletedCallback({index: index});
    }
  }

  editRow(item, index) {
    if (this.editedCallback !== undefined) {
      this.editedCallback({item: item, index: index});
    }
  }

  openRow(item, index) {
    if (this.openedCallback !== undefined) {
      this.openedCallback({item: item, index: index});
    }
  }

  arrayToString(array) {
    var res = '';
    array.forEach(element => {res += element + ', ';}, this);

    return res.slice(0, -2);
  }

  buttonClass(col, row) {
    var c = (angular.isDefined(col.class)) ? col.class : 'btn-primary';
    if (this.isDisabled(col, row)) {
      c += ' disabled';
    }
    return c;
  }

  imagePopup(url) {

    var html = '<img src="' + url + '"/>';
    this.trusted[html] || (this.trusted[html] = this.$sce.trustAsHtml(html));
    return this.trusted[html];
  }

  callCallback(col, item, index) {
    if (col.callback !== undefined) {
      col.callback(col.callbackScope, item, index);
    }
  }

  isDisabled(col, item) {
    if(col.isDisabled !== undefined) {
      return col.isDisabled(col.callbackScope, item);
    }
    return false;
  }

  translateArray(row, name, prefix) {
    var array = this.getValue(row, name);
    if (!Array.isArray(array)) {
      return "";
    }
    var result = ',';
    array.forEach(element => {
      result += ', ' + this.$filter('translate')(prefix + element);
    });
    return result.substr(2);
  }
}
